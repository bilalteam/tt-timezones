# T3
T3 (TopTal Timezones) is an application that shows time in different timezones for Toptal.

## Rules

### Admin User
Admin user is able to:

* Users CRUD:
  * Show all users.
  * Add user (Navigate to registration page).
  * Edit user info.
  * Delete user.
* Timezones CRUD:
  * See the list of the timezones.
  * Add timezones.
  * Edit timezones.
  * Delete timezones.

### Regular User
Regular user is able to:

* See the list of their own timezones.
* Add timezones.
* Edit timezones.
* Delete timezones.

## Timezones Source

The app uses [Internet Assigned Numbers Authority (IANA)](https://www.iana.org) database, ported to fluter using [TimeZone](https://pub.dev/packages/timezone) plugin. 

## Technical Aspects

### Platform
The app is a Flutter based project for Android and iOS, and can be ported to web and desktop (Windows, macOS, Linux) as well.

### State Management
The app uses [Provider](https://pub.dev/packages/provider) which is a wrapper around [InheritedWidget](https://api.flutter.dev/flutter/widgets/InheritedWidget-class.html).

### API
Firebase is used as the API layer for the project, used services:

* Firebase authentication.
* Cloud FireStore.

### Self-explanatory
No code documentation is used since the code themselves is documented by their structure and naming convention.

## Users for Testing

### Admin Users

* admin1@tttimezones.com / P@ssw0rd@123
* admin2@tttimezones.com / P@ssw0rd@123

### Regular Users

* user1@tttimezones.com / P@ssw0rd@123
* user2@tttimezones.com / P@ssw0rd@123
