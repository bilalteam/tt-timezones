import 'package:flutter_test/flutter_test.dart';
import 'package:tt_timezones/models/timezone_model.dart';

void main() {
  test(
    'Test TimezoneModel from/to map',
    () {
      Map<String, dynamic> result;

      Map<String, dynamic> map = {
        'name': 'Amman',
        'offset': 10800000,
        'timezone_name': 'Asia/Amman',
      };

      var model = TimezoneModel.fromMap(map);
      result = model.toMap();
      expect(result, map);
    },
  );
}
