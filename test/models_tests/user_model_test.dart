import 'package:flutter_test/flutter_test.dart';
import 'package:tt_timezones/models/user_model.dart';

void main() {
  test(
    'Test UserModel from/to map',
    () {
      Map<String, dynamic> result;

      Map<String, dynamic> map = {
        'first_name': 'Admin',
        'id': 'asdfgh',
        'is_active': true,
        'is_admin': true,
        'last_name': 'Two',
        'timezones': [],
      };

      var model = UserModel.fromMap(map);
      result = model.toMap();
      expect(result, map);
    },
  );
}
