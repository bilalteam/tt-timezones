import 'package:flutter_test/flutter_test.dart';
import 'package:tt_timezones/utils/offset_manager.dart';

void main() {
  test(
    'Test offsetToMinutes',
    () {
      int result;

      result = OffsetManager.offsetToMinutes(10800000);
      expect(result, 180);
    },
  );

  test(
    'Test localDurationText',
    () {
      String result;

      result = OffsetManager.localDurationText(10800000);
      expect(result, '00:00');

      result = OffsetManager.localDurationText(7200000);
      expect(result, '-1:00');

      result = OffsetManager.localDurationText(-14400000);
      expect(result, '-7:00');
    },
  );

  test(
    'Test offsetToDurationText',
    () {
      String result;

      result = OffsetManager.offsetToDurationText(10800000);
      expect(result, '+03:00');

      result = OffsetManager.offsetToDurationText(7200000);
      expect(result, '+02:00');

      result = OffsetManager.offsetToDurationText(-14400000);
      expect(result, '-4:00');
    },
  );

  test(
    'Test offsetToDuration',
    () {
      Duration result;

      result = OffsetManager.offsetToDuration(10800000);
      expect(result, Duration(hours: 3, minutes: 0));

      result = OffsetManager.offsetToDuration(7200000);
      expect(result, Duration(hours: 2, minutes: 0));

      result = OffsetManager.offsetToDuration(-14400000);
      expect(result, Duration(hours: -4, minutes: 0));
    },
  );
}
