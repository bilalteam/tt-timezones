import 'package:flutter_test/flutter_test.dart';
import 'package:tt_timezones/utils/text_validator.dart';

void main() {
  test(
    'Test validateItem for both valid or not',
    () {
      String result;

      result = TextValidator.validateItem('');
      expect(result, 'Should not be empty');

      result = TextValidator.validateItem('Anything');
      expect(result, null);
    },
  );

  test(
    'Test validateName for both valid or not',
    () {
      String result;

      result = TextValidator.validateName('');
      expect(result, 'Should not be empty');

      result = TextValidator.validateName('Amman');
      expect(result, null);
    },
  );

  test(
    'Test validatePersonName for both valid or not',
    () {
      String result;

      result = TextValidator.validatePersonName('');
      expect(result, 'Should not be empty');

      result = TextValidator.validatePersonName('Ab');
      expect(result, 'Not valid name');

      result = TextValidator.validatePersonName('A-b');
      expect(result, 'Not valid name');

      result = TextValidator.validatePersonName('56');
      expect(result, 'Not valid name');

      result = TextValidator.validatePersonName('Bilal');
      expect(result, null);
    },
  );

  test(
    'Test validateEmail for both valid or not',
    () {
      String result;

      result = TextValidator.validateEmail('');
      expect(result, 'Should not be empty');

      result = TextValidator.validateEmail('Ab');
      expect(result, 'Not valid email');

      result = TextValidator.validateEmail('hello@gmail.');
      expect(result, 'Not valid email');

      result = TextValidator.validateEmail('56');
      expect(result, 'Not valid email');

      result = TextValidator.validateEmail('bilalasmor@gmail.com');
      expect(result, null);

      result = TextValidator.validateEmail('user1@tttimezones.com');
      expect(result, null);
    },
  );

  test(
    'Test validatePassword for both valid or not',
    () {
      String result;

      result = TextValidator.validatePassword('');
      expect(result, 'Should not be empty');

      result = TextValidator.validatePassword('Ab');
      expect(result, 'Short password');

      result = TextValidator.validatePassword('49');
      expect(result, 'Short password');

      result = TextValidator.validatePassword('hiHor');
      expect(result, 'Short password');

      result = TextValidator.validatePassword('P@ssw0rd@123');
      expect(result, null);

      result = TextValidator.validatePassword('676788');
      expect(result, null);
    },
  );
}
