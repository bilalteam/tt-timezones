import 'package:flutter_test/flutter_test.dart';
import 'package:tt_timezones/utils/duration_manager.dart';

void main() {
  test(
    'Test formatDuration',
    () {
      String result;

      result = DurationManager.formatDuration(Duration(milliseconds: 10800000));
      expect(result, '03:00');

      result = DurationManager.formatDuration(Duration(milliseconds: 7200000));
      expect(result, '02:00');

      result =
          DurationManager.formatDuration(Duration(milliseconds: -14400000));
      expect(result, '-4:00');
    },
  );
}
