import 'package:flutter_test/flutter_test.dart';
import 'package:tt_timezones/utils/date_time_manager.dart';

void main() {
  test(
    'Test formatDate returns correct DATE format',
    () {
      String result;

      result = DateTimeManager.formatDate(DateTime(2021, 5, 11), 'EEEE');
      expect(result, 'Tuesday');
    },
  );

  test(
    'Test formatDate returns correct TIME format',
    () {
      String result;

      result =
          DateTimeManager.formatDate(DateTime(2021, 3, 11, 9, 56), 'E hh:mm a');
      expect(result, 'Thu 09:56 AM');

      result =
          DateTimeManager.formatDate(DateTime(2022, 4, 22, 14, 7), 'E hh:mm a');
      expect(result, 'Fri 02:07 PM');
    },
  );
}
