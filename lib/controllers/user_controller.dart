import 'package:firebase_auth/firebase_auth.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/models/user_model.dart';
import 'package:tt_timezones/services/database_service.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/services/users_service.dart';
import 'package:tt_timezones/services/working_user_service.dart';

import 'base_controller.dart';

class UserController extends BaseController {
  static const String _COLLECTION_NAME = 'users';

  UserModel currentUserModel;
  String currentEmail;
  String currentUserId;
  bool isAdminUser = false;

  final UsersService _usersService = UsersService();
  final DatabaseService _databaseService =
      new DatabaseService(_COLLECTION_NAME);
  final WorkingUserService _workingUserService = locator<WorkingUserService>();

  bool hasToken() => _usersService.hasToken();

  Future<String> login(String email, String password) async {
    changeBusy(true);

    try {
      var userCredential = await _usersService.signInWithEmailAndPassword(
          email: email, password: password);
      var firebaseUser = userCredential.user;

      currentUserId = userCredential.user.uid;

      var data = await _databaseService.get(firebaseUser.uid);

      var userModel = UserModel.fromMap(data);

      isAdminUser = userModel.isAdmin;

      return null;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return DictionaryService.ERROR_NO_USER_FOUND;
      } else if (e.code == 'wrong-password') {
        return DictionaryService.ERROR_WRONG_PASSWORD;
      } else {
        return e.message;
      }
    } finally {
      changeBusy(false);
    }
  }

  Future<bool> isCurrentUserAdmin() async {
    if (_usersService.getCurrentUserId() == null) {
      return false;
    }

    var data = await _databaseService.get(_usersService.getCurrentUserId());

    var userModel = UserModel.fromMap(data);

    if (!userModel.isActive) {
      return false;
    }

    return userModel.isAdmin;
  }

  Future<UserModel> fetchCurrentUser() async {
    if (_usersService.getCurrentUserId() == null) {
      return null;
    }

    var data = await _databaseService.get(_usersService.getCurrentUserId());

    currentEmail = _usersService.getCurrentUser().email;

    currentUserModel = UserModel.fromMap(data);

    notifyListeners();

    return currentUserModel;
  }

  Future<String> register(
    String firstName,
    String lastName,
    String email,
    String password,
    bool isAdmin,
  ) async {
    changeBusy(true);

    try {
      var userCredential = await _usersService.createUserWithEmailAndPassword(
          email: email, password: password);
      var firebaseUser = userCredential.user;

      bool isActive = !isAdmin;

      var userModel = UserModel(
        id: firebaseUser.uid,
        firstName: firstName,
        lastName: lastName,
        isActive: isActive,
        isAdmin: isAdmin,
      );

      await _databaseService.set(firebaseUser.uid, userModel);

      currentUserId = firebaseUser.uid;
      isAdminUser = isAdmin;

      return null;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return DictionaryService.ERROR_WEAK_PASSWORD;
      } else if (e.code == 'email-already-in-use') {
        return DictionaryService.ERROR_ACCOUNT_EXISTS;
      } else {
        return e.message;
      }
    } finally {
      changeBusy(false);
    }
  }

  Future logout() async {
    try {
      await _usersService.signOut();

      _workingUserService.resetUserId();

      currentUserModel = null;
      currentEmail = null;
      currentUserId = null;
      isAdminUser = false;
    } catch (_) {}
  }
}
