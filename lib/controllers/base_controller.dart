import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

abstract class BaseController with ChangeNotifier {
  bool busy = false;

  void changeBusy(bool value) {
    busy = value;

    notifyListeners();
  }
}
