import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tt_timezones/models/map_modal.dart';
import 'package:tt_timezones/models/user_model.dart';
import 'package:tt_timezones/services/database_service.dart';
import 'package:tt_timezones/services/users_service.dart';

import 'base_controller.dart';

class UsersListController extends BaseController {
  static const String _COLLECTION_NAME = 'users';

  final UsersService _usersService = new UsersService();
  final DatabaseService _databaseService = DatabaseService(_COLLECTION_NAME);

  String _currentUserId;

  String get currentUserId {
    if (_currentUserId == null) {
      _currentUserId = _usersService.getCurrentUserId();
    }

    return _currentUserId;
  }

  Stream<List<UserModel>> getItemsStream() {
    return _databaseService
        .getSnapshotsWhere('id', isNotEqualTo: currentUserId)
        .map(_itemsSnapshotToList);
  }

  List<UserModel> _itemsSnapshotToList(QuerySnapshot snapshot) {
    var list = snapshot.docs.map((doc) {
      return UserModel.fromMap(doc.data());
    }).toList();

    return list;
  }

  Future<UserModel> getItem(String id) async {
    var data = await _databaseService.get(id);

    return UserModel.fromMap(data);
  }

  Stream<DocumentSnapshot<Map<String, dynamic>>> getAsStream(String id) {
    return _databaseService.getAsStream(id);
  }

  Future add(MapModal data) async {
    changeBusy(true);

    try {
      await _databaseService.add(data);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Future block(String id) async {
    changeBusy(true);

    try {
      await _changeActive(id, false);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Future unblock(String id) async {
    changeBusy(true);

    try {
      await _changeActive(id, true);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Future _changeActive(String id, bool isActive) async {
    changeBusy(true);

    try {
      var user = await getItem(id);

      user.isActive = false;

      await editItem(id, user);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Future invertActive(String id) async {
    changeBusy(true);

    try {
      var user = await getItem(id);

      user.isActive = !user.isActive;

      await editItem(id, user);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Future editItem(String id, MapModal data) async {
    changeBusy(true);

    try {
      await _databaseService.set(id, data);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Future delete(String id) async {
    changeBusy(true);

    try {
      await _databaseService.delete(id);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }
}
