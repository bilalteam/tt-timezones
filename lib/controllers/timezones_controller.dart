import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/models/map_modal.dart';
import 'package:tt_timezones/services/database_service.dart';
import 'package:tt_timezones/services/users_service.dart';
import 'package:tt_timezones/services/working_user_service.dart';

import 'base_controller.dart';
import 'users_list_controller.dart';

class TimezonesController extends BaseController {
  final UsersService _usersService = new UsersService();
  DatabaseService _databaseService;
  final WorkingUserService _workingUserService = locator<WorkingUserService>();
  final UsersListController _usersListController =
      locator<UsersListController>();

  String get currentUserId => _usersService.getCurrentUserId();

  Stream<DocumentSnapshot> getAsStream(String id) {
    return _databaseService.getAsStream(id);
  }

  Stream<DocumentSnapshot<Map<String, dynamic>>> getWorkingUserAsStream() {
    return _usersListController.getAsStream(_workingUserService.userId);
  }

  Future add(MapModal data) async {
    changeBusy(true);

    try {
      var user = await _usersListController.getItem(_workingUserService.userId);

      user.addTimezone(data);

      await _usersListController.editItem(_workingUserService.userId, user);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Future edit(String id, MapModal data) async {
    changeBusy(true);

    try {
      var user = await _usersListController.getItem(_workingUserService.userId);

      user.editTimezone(id, data);

      await _usersListController.editItem(_workingUserService.userId, user);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Future delete(String id) async {
    changeBusy(true);

    try {
      var user = await _usersListController.getItem(_workingUserService.userId);

      user.deleteTimezone(id);

      await _usersListController.editItem(_workingUserService.userId, user);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }
}
