import 'package:timezone/timezone.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/utils/offset_manager.dart';

extension LocationExtension on Location {
  String toLocalDurationText() {
    int offset = this.currentTimeZone.offset;

    String text = OffsetManager.localDurationText(offset);
    String localTitle = DictionaryService.LOCAL;

    return '$localTitle $text';
  }

  String toGmtDurationText() {
    int offset = this.currentTimeZone.offset;

    String text = OffsetManager.offsetToDurationText(offset);
    String gmtTitle = DictionaryService.GMT;

    return '$gmtTitle $text';
  }
}
