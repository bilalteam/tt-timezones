import 'package:get_it/get_it.dart';

import 'controllers/timezones_controller.dart';
import 'controllers/user_controller.dart';
import 'controllers/users_list_controller.dart';
import 'services/working_user_service.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => WorkingUserService());

  locator.registerFactory<UserController>(() => UserController());
  locator.registerFactory<UsersListController>(() => UsersListController());
  locator.registerFactory<TimezonesController>(() => TimezonesController());
}
