import 'package:tt_timezones/models/timezone_model.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/utils/map_parser.dart';

import 'map_modal.dart';

class UserModel implements MapModal {
  String id;
  String firstName;
  String lastName;
  bool isActive;
  bool isAdmin;
  List<TimezoneModel> timezones;

  UserModel({
    this.id = '',
    this.firstName = '',
    this.lastName = '',
    this.isActive = false,
    this.isAdmin = false,
  }) {
    this.timezones = [];
  }

  UserModel.fromMap(Map<String, dynamic> map) {
    id = MapParser.parseStringWithDefault(map, 'id', '');
    firstName = MapParser.parseStringWithDefault(map, 'first_name', '');
    lastName = MapParser.parseStringWithDefault(map, 'last_name', '');
    isActive = MapParser.parseBoolWithDefault(map, 'is_active', false);
    isAdmin = MapParser.parseBoolWithDefault(map, 'is_admin', false);

    timezones = [];
    if (MapParser.hasValue(map, 'timezones')) {
      MapParser.parseDynamic(map, 'timezones').forEach((v) {
        timezones.add(TimezoneModel.fromMap(v));
      });

      timezones.sort();
    }
  }

  @override
  Map<String, dynamic> toMap() {
    List<Map<String, dynamic>> timezonesListMaps = [];
    timezones.forEach((element) {
      timezonesListMaps.add(element.toMap());
    });

    return {
      'id': id,
      'first_name': firstName,
      'last_name': lastName,
      'is_active': isActive,
      'is_admin': isAdmin,
      'timezones': timezonesListMaps,
    };
  }

  String get firstLetter {
    if (firstName == null || firstName.isEmpty) {
      return DictionaryService.EMPTY;
    }

    return firstName[0].toUpperCase();
  }

  String get fullName {
    String text = firstName.trim() + ' ' + lastName.trim();

    return text;
  }

  String get userType =>
      isAdmin ? DictionaryService.ADMIN : DictionaryService.REGULAR;

  void addTimezone(TimezoneModel model) {
    if (!timezones.any((element) => element == model)) {
      timezones.add(model);
    }
  }

  void editTimezone(String id, TimezoneModel model) {
    var found = timezones.firstWhere(
        (element) => element.timezoneName.toLowerCase() == id.toLowerCase(),
        orElse: () => null);

    if (found == null) {
      return;
    }

    if (model.timezoneName.toLowerCase() != id.toLowerCase() &&
        timezones.any((element) =>
            element.timezoneName.toLowerCase() ==
            model.timezoneName.toLowerCase())) {
      return;
    }

    found.name = model.name;
    found.timezoneName = model.timezoneName;
    found.offset = model.offset;
  }

  void deleteTimezone(String id) {
    timezones.removeWhere(
        (element) => element.timezoneName.toLowerCase() == id.toLowerCase());
  }
}
