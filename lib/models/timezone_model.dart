import 'package:tt_timezones/services/timezones_services.dart';
import 'package:tt_timezones/utils/date_time_manager.dart';
import 'package:tt_timezones/utils/map_parser.dart';

import 'map_modal.dart';

class TimezoneModel implements MapModal, Comparable<TimezoneModel> {
  String name;
  String timezoneName;
  int offset;

  TimezoneModel({
    this.name = '',
    this.timezoneName = '',
    this.offset = 0,
  });

  TimezoneModel.fromMap(Map<String, dynamic> map) {
    name = MapParser.parseStringWithDefault(map, 'name', '');
    timezoneName = MapParser.parseStringWithDefault(map, 'timezone_name', '');
    offset = MapParser.parseIntWithDefault(map, 'offset', 0);
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'timezone_name': timezoneName,
      'offset': offset,
    };
  }

  String getFormattedTimeThere() {
    var there = getTimeThere();

    return DateTimeManager.formatDate(there, 'E hh:mm a');
  }

  String getFormatted24TimeThere() {
    var there = getTimeThere();

    return DateTimeManager.formatDate(there, 'HH:mm');
  }

  DateTime getTimeThere() {
    var now = DateTime.now();

    var timeThere = TimezonesServices.convertByLocationName(now, timezoneName);

    var there = DateTime(
      timeThere.year,
      timeThere.month,
      timeThere.day,
      timeThere.hour,
      timeThere.minute,
    );

    return there;
  }

  @override
  int get hashCode => timezoneName.hashCode;

  bool operator ==(o) => timezoneName == o.timezoneName;

  @override
  int compareTo(TimezoneModel other) => offset.compareTo(other.offset);
}
