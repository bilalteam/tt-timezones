import 'package:flutter/material.dart';
import 'package:tt_timezones/controllers/user_controller.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/view/pages/home_page.dart';

import 'user/login_page.dart';

class WelcomePage extends StatefulWidget {
  WelcomePage({Key key}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  UserController _controller = locator<UserController>();

  @override
  Widget build(BuildContext context) {
    if (!_controller.hasToken()) {
      return LoginPage();
    } else {
      return HomePage();
    }
  }
}
