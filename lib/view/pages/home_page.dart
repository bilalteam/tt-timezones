import 'package:flutter/material.dart';
import 'package:tt_timezones/controllers/user_controller.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/models/user_model.dart';
import 'package:tt_timezones/view/pages/user/deactivated_user_page.dart';
import 'package:tt_timezones/view/shared_widgets/center_progress.dart';

import 'admin/admin_homepage.dart';
import 'regular/regular_homepage.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  UserController _controller = locator<UserController>();

  Future<UserModel> _future;

  @override
  void initState() {
    super.initState();

    _future = _controller.fetchCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _future,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return _onOk(snapshot);
        } else {
          return _buildUi();
        }
      },
    );
  }

  Widget _onOk(AsyncSnapshot snapshot) {
    try {
      var userModel = snapshot.data as UserModel;

      return _tryOnOk(userModel);
    } catch (_) {
      return _buildUi();
    }
  }

  Widget _tryOnOk(UserModel userModel) {
    if (!userModel.isActive) {
      return DeactivatedUserPage();
    } else if (userModel.isAdmin) {
      return AdminHomepage();
    } else {
      return RegularHomepage();
    }
  }

  Widget _buildUi() {
    return SafeArea(
      child: Scaffold(
        body: CenterProgress(),
      ),
    );
  }
}
