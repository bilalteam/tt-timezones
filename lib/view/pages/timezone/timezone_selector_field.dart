import 'package:flutter/material.dart';
import 'package:timezone/timezone.dart';

import 'timezone_selector.dart';

class TimezoneSelectorField extends FormField<Location> {
  TimezoneSelectorField({
    FormFieldSetter<Location> onSaved,
    FormFieldValidator<Location> validator,
    Location initialValue,
    @required String title,
    ValueChanged<Location> onSelected,
  }) : super(
          onSaved: onSaved,
          validator: validator,
          initialValue: initialValue,
          builder: (FormFieldState<Location> state) => _buildBuilderWidget(
            state,
            initialValue,
            title,
            onSelected,
          ),
        );

  static Widget _buildBuilderWidget(
    FormFieldState<Location> state,
    Location initialValue,
    String title,
    ValueChanged<Location> onSelected,
  ) {
    return InputDecorator(
      decoration: InputDecoration(
        labelText: title,
        errorText: state.hasError ? state.errorText : null,
        border: InputBorder.none,
      ),
      child: TimezoneSelector(
        initialValue: initialValue,
        onSelected: (value) {
          state.didChange(value);

          onSelected?.call(value);
        },
      ),
    );
  }
}
