import 'package:flutter/material.dart';
import 'package:timezone/timezone.dart';
import 'package:tt_timezones/services/timezones_services.dart';

class TimezoneSelector extends StatefulWidget {
  final Location initialValue;
  final Function(Location value) onSelected;

  TimezoneSelector({
    Key key,
    this.initialValue,
    this.onSelected,
  }) : super(key: key);

  @override
  _TimezoneSelectorState createState() => _TimezoneSelectorState();
}

class _TimezoneSelectorState extends State<TimezoneSelector> {
  Location _selectedItem;

  @override
  Widget build(BuildContext context) {
    List<Location> locations = [];

    for (var item in TimezonesServices.locations.entries) {
      locations.add(item.value);
    }

    return Autocomplete<Location>(
      optionsBuilder: (TextEditingValue textEditingValue) {
        if (textEditingValue.text == '') {
          return locations;
        }

        return locations
            .where((Location element) => element.name
                .toLowerCase()
                .contains(textEditingValue.text.toLowerCase()))
            .toList();
      },
      displayStringForOption: (Location element) => element.name,
      onSelected: (option) {
        setState(() {
          _selectedItem = option;
        });

        widget.onSelected?.call(_selectedItem);
      },
    );
  }
}
