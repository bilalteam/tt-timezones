import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timezone/timezone.dart';
import 'package:tt_timezones/controllers/timezones_controller.dart';
import 'package:tt_timezones/extensions/location_extension.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/models/timezone_model.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/services/timezones_services.dart';
import 'package:tt_timezones/utils/app_spaces.dart';
import 'package:tt_timezones/utils/dialogs_manager.dart';
import 'package:tt_timezones/utils/text_validator.dart';
import 'package:tt_timezones/view/shared_widgets/app_button.dart';
import 'package:tt_timezones/view/shared_widgets/app_consumer.dart';
import 'package:tt_timezones/view/shared_widgets/dismissible_keyboard.dart';

import 'timezone_selector_field.dart';

class TimezoneDetail extends StatefulWidget {
  final bool editing;
  final String id;
  final TimezoneModel initialValue;
  final Function onSaved;

  TimezoneDetail({
    Key key,
    this.editing = false,
    this.id,
    this.initialValue,
    this.onSaved,
  }) : super(key: key);

  @override
  _TimezoneDetailState createState() => _TimezoneDetailState();
}

class _TimezoneDetailState extends State<TimezoneDetail> {
  TimezonesController _controller = locator<TimezonesController>();

  TimezoneModel _currentItem;

  final _formKey = GlobalKey<FormState>();

  final TextEditingController _nameController = new TextEditingController();
  Location _location;

  @override
  void initState() {
    super.initState();

    if (widget.editing) {
      _currentItem = widget.initialValue;

      _nameController.text = _currentItem.name;
      _location = TimezonesServices.getLocationByName(
        _currentItem.timezoneName,
      );
    } else {
      _currentItem = TimezoneModel();
    }
  }

  @override
  Widget build(BuildContext context) {
    var consumer = ChangeNotifierProvider<TimezonesController>(
      create: (context) => _controller,
      child: Consumer<TimezonesController>(
        builder: (context, model, child) {
          return AppConsumer(
            value: model,
            builder: (context, value, child) {
              return _buildUi();
            },
          );
        },
      ),
    );

    return DismissibleKeyboard(
      context: context,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          consumer,
        ],
      ),
    );
  }

  Widget _buildUi() {
    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: _buildFields(),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildFields() {
    return [
      TextFormField(
        decoration: InputDecoration(
          labelText: DictionaryService.CITY_NAME,
          suffixIcon: Icon(Icons.location_city),
        ),
        controller: _nameController,
        keyboardType: TextInputType.name,
        validator: (value) => TextValidator.validateName(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      if (_hasSavedLocation()) _buildSavedLocation(),
      if (_hasSavedLocation()) SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      TimezoneSelectorField(
        title: DictionaryService.TIMEZONE,
        initialValue: _location,
        validator: (value) => TextValidator.validateItem(value),
        onSelected: (value) {
          setState(() {
            _location = value;
          });
        },
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      _buildDifference(),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      AppButton(
        text: DictionaryService.SAVE,
        onPressed: () async {
          await _save();
        },
      ),
    ];
  }

  bool _hasSavedLocation() {
    return widget.editing &&
        widget.initialValue != null &&
        widget.initialValue.timezoneName != null;
  }

  Widget _buildSavedLocation() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: DictionaryService.SAVED_LOCATION,
        suffixIcon: Icon(Icons.location_on),
      ),
      initialValue: widget.initialValue.timezoneName,
      readOnly: true,
    );
  }

  Widget _buildDifference() {
    if (_location == null) {
      return Text(DictionaryService.EMPTY);
    }

    String gmt = _location.toGmtDurationText();
    String local = _location.toLocalDurationText();
    String text = '$gmt $local';

    return Text(text);
  }

  Future _save() async {
    if (!_checkForm()) {
      return;
    }

    if (_location == null) {
      return;
    }

    _currentItem.name = _nameController.text;
    _currentItem.timezoneName = _location.name;
    _currentItem.offset = _location.currentTimeZone.offset;

    String error;

    if (widget.editing) {
      error = await _controller.edit(
        widget.id,
        _currentItem,
      );
    } else {
      error = await _controller.add(_currentItem);
    }

    if (error != null) {
      await DialogsManager.showOkDialog(context: context, message: error);

      return;
    }

    widget.onSaved?.call();
  }

  bool _checkForm() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      return true;
    } else {
      return false;
    }
  }
}
