import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tt_timezones/controllers/timezones_controller.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/models/timezone_model.dart';
import 'package:tt_timezones/models/user_model.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/utils/app_spaces.dart';
import 'package:tt_timezones/utils/dialogs_manager.dart';
import 'package:tt_timezones/utils/navigation_manager.dart';
import 'package:tt_timezones/view/pages/timezone/timezone_info.dart';
import 'package:tt_timezones/view/pages/timezone/timezone_list_item_widget.dart';
import 'package:tt_timezones/view/shared_widgets/actions_app_bar.dart';
import 'package:tt_timezones/view/shared_widgets/app_consumer.dart';
import 'package:tt_timezones/view/shared_widgets/app_drawer.dart';
import 'package:tt_timezones/view/shared_widgets/center_progress.dart';
import 'package:tt_timezones/view/shared_widgets/no_items.dart';

import 'timezone_detail.dart';

class TimezonesListPage extends StatefulWidget {
  final bool noDrawer;

  TimezonesListPage({
    Key key,
    @required this.noDrawer,
  }) : super(key: key);

  @override
  _TimezonesListPageState createState() => _TimezonesListPageState();
}

class _TimezonesListPageState extends State<TimezonesListPage> {
  TimezonesController _controller = locator<TimezonesController>();

  @override
  Widget build(BuildContext context) {
    var consumer = ChangeNotifierProvider<TimezonesController>(
      create: (context) => _controller,
      child: Consumer<TimezonesController>(
        builder: (context, model, child) {
          return AppConsumer(
            value: model,
            builder: (context, value, child) {
              return _buildUi();
            },
          );
        },
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: ActionsAppBar(title: DictionaryService.TIMEZONES),
        body: consumer,
        drawer: widget.noDrawer ? null : AppDrawer(),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            await _add();
          },
          tooltip: DictionaryService.ADD,
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  Widget _buildUi() {
    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: StreamBuilder<DocumentSnapshot<Map<String, dynamic>>>(
        stream: _controller.getWorkingUserAsStream(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return _buildWithSnapshot(snapshot.data);
          } else {
            return CenterProgress();
          }
        },
      ),
    );
  }

  Widget _buildWithSnapshot(DocumentSnapshot<Map<String, dynamic>> data) {
    var map = data.data();

    var user = UserModel.fromMap(map);

    return _buildWithUser(user);
  }

  Widget _buildWithUser(UserModel user) {
    return _buildItemsList(user.timezones);
  }

  Widget _buildItemsList(List<TimezoneModel> list) {
    return list.isEmpty
        ? NoItems()
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              var item = list[index];

              return TimezoneListItemWidget(
                item: item,
                onSelected: (item) async {
                  await _edit(item);
                },
                onDeleted: (id) async {
                  await _controller.delete(id);
                },
              );
            },
          );
  }

  Future _add() async {
    var child = TimezoneDetail(
      editing: false,
      onSaved: () {
        _pop();
      },
    );

    await DialogsManager.showWidgetDialog(
      context: context,
      child: child,
    );
  }

  Future _edit(TimezoneModel item) async {
    await NavigationManager.push(context, TimezoneInfo(item: item));
  }

  void _pop() {
    NavigationManager.pop(context);
  }
}
