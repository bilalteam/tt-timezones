import 'package:flutter/material.dart';
import 'package:tt_timezones/models/timezone_model.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/utils/app_spaces.dart';
import 'package:tt_timezones/utils/dialogs_manager.dart';
import 'package:tt_timezones/utils/navigation_manager.dart';
import 'package:tt_timezones/utils/offset_manager.dart';
import 'package:tt_timezones/view/shared_widgets/actions_app_bar.dart';
import 'package:tt_timezones/view/shared_widgets/clock.dart';

import 'timezone_detail.dart';

class TimezoneInfo extends StatefulWidget {
  final TimezoneModel item;

  TimezoneInfo({
    Key key,
    @required this.item,
  }) : super(key: key);

  @override
  _TimezoneInfoState createState() => _TimezoneInfoState();
}

class _TimezoneInfoState extends State<TimezoneInfo> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: ActionsAppBar(title: widget.item.name),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            await _edit();
          },
          tooltip: DictionaryService.EDIT,
          child: Icon(Icons.edit),
        ),
        body: SingleChildScrollView(
          child: _buildUi(),
        ),
      ),
    );
  }

  Widget _buildUi() {
    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: Column(
        children: [
          Clock(dateTime: widget.item.getTimeThere()),
          SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
          _buildHeadline(),
          SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
          _buildInfoCard(),
        ],
      ),
    );
  }

  Widget _buildHeadline() {
    return Text(
      widget.item.timezoneName,
      style: Theme.of(context).textTheme.headline5,
    );
  }

  Widget _buildInfoCard() {
    return Row(
      children: [
        Expanded(
          child: Card(
            margin: EdgeInsets.symmetric(
              horizontal: AppSpaces.HALF_SPACE,
              vertical: AppSpaces.QUARTER_SPACE,
            ),
            child: Padding(
              padding: const EdgeInsets.all(AppSpaces.HALF_SPACE),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildTimeThere(),
                  SizedBox(height: AppSpaces.MAIN_SPACE),
                  _buildGmtTimeThere(),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildTimeThere() {
    String offsetText = OffsetManager.localDurationText(widget.item.offset);
    String localTitle = DictionaryService.LOCAL;

    String text = '$localTitle $offsetText';
    return Text(
      text,
      style: Theme.of(context).textTheme.subtitle2,
    );
  }

  Widget _buildGmtTimeThere() {
    String offsetText = OffsetManager.offsetToDurationText(widget.item.offset);
    String gmtTitle = DictionaryService.GMT;

    String text = '$gmtTitle $offsetText';
    return Text(
      text,
      style: Theme.of(context).textTheme.subtitle2,
    );
  }

  Future _edit() async {
    var child = TimezoneDetail(
      editing: true,
      id: widget.item.timezoneName,
      initialValue: widget.item,
      onSaved: () {
        _popTwice();
      },
    );

    await DialogsManager.showWidgetDialog(
      context: context,
      child: child,
    );
  }

  void _popTwice() {
    NavigationManager.pop(context);
    NavigationManager.pop(context);
  }
}
