import 'package:flutter/material.dart';
import 'package:tt_timezones/models/timezone_model.dart';
import 'package:tt_timezones/utils/app_spaces.dart';

class TimezoneListItemWidget extends StatefulWidget {
  final TimezoneModel item;
  final Function(TimezoneModel item) onSelected;
  final Function(String id) onDeleted;

  TimezoneListItemWidget({
    Key key,
    @required this.item,
    this.onSelected,
    this.onDeleted,
  }) : super(key: key);

  @override
  _TimezoneListItemWidgetState createState() => _TimezoneListItemWidgetState();
}

class _TimezoneListItemWidgetState extends State<TimezoneListItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(
        horizontal: AppSpaces.HALF_SPACE,
        vertical: AppSpaces.QUARTER_SPACE,
      ),
      child: ListTile(
        contentPadding: const EdgeInsets.all(AppSpaces.HALF_SPACE),
        title: Text(
          widget.item.name,
          style: Theme.of(context).textTheme.headline5,
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.item.timezoneName,
              style: Theme.of(context).textTheme.subtitle1,
            ),
            SizedBox(height: AppSpaces.HALF_SPACE),
            _buildTimeThere(widget.item),
          ],
        ),
        leading: CircleAvatar(
          backgroundColor: Theme.of(context).primaryColor,
          child: Icon(Icons.timelapse),
        ),
        trailing: _buildDelete(widget.item.timezoneName),
        onTap: () {
          widget.onSelected(widget.item);
        },
      ),
    );
  }

  Widget _buildTimeThere(TimezoneModel item) {
    String text = item.getFormattedTimeThere();

    return Text(
      text,
      style: Theme.of(context).textTheme.subtitle2,
    );
  }

  Widget _buildDelete(String id) {
    return InkWell(
      child: Icon(Icons.delete),
      onTap: () {
        widget.onDeleted(id);
      },
    );
  }
}
