import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tt_timezones/controllers/users_list_controller.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/models/user_model.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/services/working_user_service.dart';
import 'package:tt_timezones/utils/app_spaces.dart';
import 'package:tt_timezones/utils/dialogs_manager.dart';
import 'package:tt_timezones/utils/navigation_manager.dart';
import 'package:tt_timezones/view/pages/timezone/timezones_list_page.dart';
import 'package:tt_timezones/view/pages/user/registration_page.dart';
import 'package:tt_timezones/view/pages/users_list/user_detail.dart';
import 'package:tt_timezones/view/shared_widgets/actions_app_bar.dart';
import 'package:tt_timezones/view/shared_widgets/app_consumer.dart';
import 'package:tt_timezones/view/shared_widgets/app_drawer.dart';
import 'package:tt_timezones/view/shared_widgets/center_progress.dart';
import 'package:tt_timezones/view/shared_widgets/no_items.dart';

import 'user_list_item_widget.dart';

class UsersListPage extends StatefulWidget {
  UsersListPage({Key key}) : super(key: key);

  @override
  _UsersListPageState createState() => _UsersListPageState();
}

class _UsersListPageState extends State<UsersListPage> {
  UsersListController _controller = locator<UsersListController>();
  WorkingUserService _workingUserService = locator<WorkingUserService>();

  @override
  Widget build(BuildContext context) {
    var consumer = ChangeNotifierProvider<UsersListController>(
      create: (context) => _controller,
      child: Consumer<UsersListController>(
        builder: (context, model, child) {
          return AppConsumer(
            value: model,
            builder: (context, value, child) {
              return _buildUi();
            },
          );
        },
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: ActionsAppBar(title: DictionaryService.USERS),
        drawer: AppDrawer(),
        body: consumer,
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            await _add();
          },
          tooltip: DictionaryService.ADD,
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  Widget _buildUi() {
    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: StreamBuilder<List<UserModel>>(
        stream: _controller.getItemsStream(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return _buildWithSnapshot(snapshot.data);
          } else {
            return CenterProgress();
          }
        },
      ),
    );
  }

  Widget _buildWithSnapshot(List<UserModel> data) {
    return _buildItemsList(data);
  }

  Widget _buildItemsList(List<UserModel> list) {
    return list.isEmpty
        ? NoItems()
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              var item = list[index];

              return UserListItemWidget(
                item: item,
                onSelected: (item) async {
                  await _select(item);
                },
                onEdited: (item) async {
                  await _edit(item);
                },
                onActiveChanged: (id) async {
                  await _controller.invertActive(id);
                },
              );
            },
          );
  }

  Future _add() async {
    await DialogsManager.showYesNoDialog(
      context: context,
      message: DictionaryService.ADD_USER_ALERT,
      action: () async {
        await NavigationManager.pushClearBack(context, RegistrationPage());
      },
    );
  }

  Future _select(UserModel item) async {
    if (item.isAdmin) {
      await _selectAdminUser(item);
    } else {
      await _selectRegularUser(item);
    }
  }

  Future _selectAdminUser(UserModel item) async {
    await DialogsManager.showOkDialog(
      context: context,
      message: DictionaryService.SELECT_ADMIN_USER,
    );
  }

  Future _selectRegularUser(UserModel item) async {
    _workingUserService.selectUserId(item.id);

    await NavigationManager.push(
      context,
      TimezonesListPage(
        noDrawer: true,
      ),
    );
  }

  Future _edit(UserModel item) async {
    var child = UserDetail(
      item: item,
      onSaved: () {
        _pop();
      },
    );

    await DialogsManager.showWidgetDialog(
      context: context,
      child: child,
    );
  }

  void _pop() {
    NavigationManager.pop(context);
  }
}
