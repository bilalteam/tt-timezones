import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tt_timezones/controllers/users_list_controller.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/models/user_model.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/utils/app_spaces.dart';
import 'package:tt_timezones/utils/dialogs_manager.dart';
import 'package:tt_timezones/utils/text_validator.dart';
import 'package:tt_timezones/view/shared_widgets/app_button.dart';
import 'package:tt_timezones/view/shared_widgets/app_consumer.dart';
import 'package:tt_timezones/view/shared_widgets/dismissible_keyboard.dart';
import 'package:tt_timezones/view/shared_widgets/user_type_widget.dart';

class UserDetail extends StatefulWidget {
  final UserModel item;
  final Function onSaved;

  UserDetail({
    Key key,
    @required this.item,
    this.onSaved,
  }) : super(key: key);

  @override
  _UserDetailState createState() => _UserDetailState();
}

class _UserDetailState extends State<UserDetail> {
  UsersListController _controller = locator<UsersListController>();

  final _formKey = GlobalKey<FormState>();

  UserModel _currentItem;

  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  bool _isAdmin = false;
  bool _isActive = false;

  @override
  void initState() {
    super.initState();

    _currentItem = widget.item;

    _firstNameController.text = _currentItem.firstName;
    _lastNameController.text = _currentItem.lastName;
    _isAdmin = _currentItem.isAdmin;
    _isActive = _currentItem.isActive;
  }

  @override
  Widget build(BuildContext context) {
    var consumer = ChangeNotifierProvider<UsersListController>(
      create: (context) => _controller,
      child: Consumer<UsersListController>(
        builder: (context, model, child) {
          return AppConsumer(
            value: model,
            builder: (context, value, child) {
              return _buildUi();
            },
          );
        },
      ),
    );

    return DismissibleKeyboard(
      context: context,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          consumer,
        ],
      ),
    );
  }

  Widget _buildUi() {
    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: _buildFields(),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildFields() {
    return [
      TextFormField(
        decoration: InputDecoration(
          labelText: DictionaryService.FIRST_NAME,
          suffixIcon: Icon(Icons.person),
        ),
        controller: _firstNameController,
        keyboardType: TextInputType.name,
        validator: (value) => TextValidator.validateName(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      TextFormField(
        decoration: InputDecoration(
          labelText: DictionaryService.LAST_NAME,
          suffixIcon: Icon(Icons.perm_identity),
        ),
        controller: _lastNameController,
        keyboardType: TextInputType.name,
        validator: (value) => TextValidator.validateName(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      Text(DictionaryService.USER_TYPE),
      SizedBox(height: AppSpaces.MAIN_SPACE),
      UserTypeWidget(
        isAdmin: widget.item.isAdmin,
        onSelected: (isAdmin) {
          _isAdmin = isAdmin;
        },
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      SwitchListTile(
        title: Text(DictionaryService.IS_ACTIVE),
        value: _isActive,
        onChanged: (value) {
          setState(() {
            _isActive = value;
          });
        },
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      AppButton(
        text: DictionaryService.SAVE,
        onPressed: () async {
          await _save();
        },
      ),
    ];
  }

  Future _save() async {
    if (!_checkForm()) {
      return;
    }

    _currentItem.firstName = _firstNameController.text;
    _currentItem.lastName = _lastNameController.text;
    _currentItem.isAdmin = _isAdmin;
    _currentItem.isActive = _isActive;

    String error = await _controller.editItem(_currentItem.id, _currentItem);

    if (error != null) {
      await DialogsManager.showOkDialog(context: context, message: error);

      return;
    }

    widget.onSaved?.call();
  }

  bool _checkForm() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      return true;
    } else {
      return false;
    }
  }
}
