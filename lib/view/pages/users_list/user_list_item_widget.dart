import 'package:flutter/material.dart';
import 'package:tt_timezones/models/user_model.dart';
import 'package:tt_timezones/utils/app_colors.dart';
import 'package:tt_timezones/utils/app_spaces.dart';

class UserListItemWidget extends StatefulWidget {
  final UserModel item;
  final Function(UserModel item) onSelected;
  final Function(UserModel item) onEdited;
  final Function(String id) onActiveChanged;

  UserListItemWidget({
    Key key,
    @required this.item,
    this.onSelected,
    this.onEdited,
    this.onActiveChanged,
  }) : super(key: key);

  @override
  _UserListItemWidgetState createState() => _UserListItemWidgetState();
}

class _UserListItemWidgetState extends State<UserListItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(
        horizontal: AppSpaces.HALF_SPACE,
        vertical: AppSpaces.QUARTER_SPACE,
      ),
      child: ListTile(
        contentPadding: const EdgeInsets.all(AppSpaces.HALF_SPACE),
        title: _buildUserInfo(context),
        subtitle: Text(
          widget.item.userType,
          style: Theme.of(context).textTheme.subtitle1,
        ),
        leading: _buildEdit(),
        trailing: _buildChangeActive(),
        onTap: () {
          widget.onSelected(widget.item);
        },
      ),
    );
  }

  Widget _buildUserInfo(BuildContext context) {
    Color color = widget.item.isActive ? null : AppColors.NEGATIVE_COLOR;

    return Text(
      widget.item.fullName,
      style: Theme.of(context).textTheme.headline5.copyWith(
            color: color,
          ),
    );
  }

  Widget _buildEdit() {
    return InkWell(
      child: Icon(Icons.edit),
      onTap: () {
        widget.onEdited(widget.item);
      },
    );
  }

  Widget _buildChangeActive() {
    IconData icon;
    Color color;

    if (widget.item.isActive) {
      icon = Icons.block;
      color = AppColors.NEGATIVE_COLOR;
    } else {
      icon = Icons.public;
      color = AppColors.POSITIVE_COLOR;
    }

    return InkWell(
      child: Icon(
        icon,
        color: color,
      ),
      onTap: () {
        widget.onActiveChanged(widget.item.id);
      },
    );
  }
}
