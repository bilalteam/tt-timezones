import 'package:flutter/material.dart';
import 'package:tt_timezones/view/pages/users_list/users_list_page.dart';

class AdminHomepage extends StatefulWidget {
  AdminHomepage({Key key}) : super(key: key);

  @override
  _AdminHomepageState createState() => _AdminHomepageState();
}

class _AdminHomepageState extends State<AdminHomepage> {
  @override
  Widget build(BuildContext context) {
    return UsersListPage();
  }
}
