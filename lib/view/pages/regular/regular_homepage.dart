import 'package:flutter/material.dart';
import 'package:tt_timezones/view/pages/timezone/timezones_list_page.dart';

class RegularHomepage extends StatefulWidget {
  RegularHomepage({Key key}) : super(key: key);

  @override
  _RegularHomepageState createState() => _RegularHomepageState();
}

class _RegularHomepageState extends State<RegularHomepage> {
  @override
  Widget build(BuildContext context) {
    return TimezonesListPage(
      noDrawer: false,
    );
  }
}
