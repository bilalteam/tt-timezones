import 'package:flutter/material.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/utils/app_spaces.dart';
import 'package:tt_timezones/utils/navigation_manager.dart';
import 'package:tt_timezones/view/pages/user/login_page.dart';
import 'package:tt_timezones/view/shared_widgets/app_button.dart';

class DeactivatedUserPage extends StatefulWidget {
  DeactivatedUserPage({Key key}) : super(key: key);

  @override
  _DeactivatedUserPageState createState() => _DeactivatedUserPageState();
}

class _DeactivatedUserPageState extends State<DeactivatedUserPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(DictionaryService.APP_NAME),
        ),
        body: Container(
          padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
          child: _buildUi(),
        ),
      ),
    );
  }

  Widget _buildUi() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildIcon(),
          SizedBox(height: AppSpaces.MAIN_SPACE),
          Text(
            DictionaryService.ERROR_ACCOUNT_DEACTIVATED,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
          _buildAction(),
        ],
      ),
    );
  }

  Widget _buildIcon() {
    return Icon(
      Icons.warning,
      size: AppSpaces.LARGE_ICON,
      color: Theme.of(context).accentColor,
    );
  }

  Widget _buildAction() {
    return Row(
      children: [
        Expanded(
          child: AppButton(
            text: DictionaryService.BACK_TO_LOGIN,
            onPressed: () async {
              await _navigateToLogin();
            },
          ),
        ),
      ],
    );
  }

  Future _navigateToLogin() async {
    await NavigationManager.pushClearBack(context, LoginPage());
  }
}
