import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tt_timezones/controllers/user_controller.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/utils/app_spaces.dart';
import 'package:tt_timezones/utils/debugging_manager.dart';
import 'package:tt_timezones/utils/dialogs_manager.dart';
import 'package:tt_timezones/utils/navigation_manager.dart';
import 'package:tt_timezones/utils/text_validator.dart';
import 'package:tt_timezones/view/shared_widgets/app_button.dart';
import 'package:tt_timezones/view/shared_widgets/app_consumer.dart';
import 'package:tt_timezones/view/shared_widgets/dismissible_keyboard.dart';

import '../home_page.dart';
import 'registration_page.dart';

class LoginPage extends StatefulWidget {
  LoginPage({
    Key key,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  UserController _controller = locator<UserController>();

  final _formKey = GlobalKey<FormState>();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _emailController.text = DebuggingManager.USERNAME;
    _passwordController.text = DebuggingManager.PASSWORD;

    _controller.logout().then((value) {});
  }

  @override
  Widget build(BuildContext context) {
    var consumer = ChangeNotifierProvider<UserController>(
      create: (context) => _controller,
      child: Consumer<UserController>(
        builder: (context, model, child) {
          return AppConsumer(
            value: model,
            builder: (context, value, child) {
              return _buildUi();
            },
          );
        },
      ),
    );

    return DismissibleKeyboard(
      context: context,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text(DictionaryService.LOGIN),
          ),
          body: consumer,
        ),
      ),
    );
  }

  Widget _buildUi() {
    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: _buildFields() + _buildButtons(),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildFields() {
    return [
      TextFormField(
        decoration: InputDecoration(
          labelText: DictionaryService.EMAIL,
          suffixIcon: Icon(Icons.email),
        ),
        controller: _emailController,
        keyboardType: TextInputType.emailAddress,
        validator: (value) => TextValidator.validateEmail(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      TextFormField(
        decoration: InputDecoration(
          labelText: DictionaryService.PASSWORD,
          suffixIcon: Icon(Icons.security),
        ),
        controller: _passwordController,
        keyboardType: TextInputType.text,
        obscureText: true,
        validator: (value) => TextValidator.validatePassword(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
    ];
  }

  List<Widget> _buildButtons() {
    return [
      AppButton(
        text: DictionaryService.LOGIN,
        onPressed: () async {
          await _login();
        },
      ),
      TextButton(
        child: Text(DictionaryService.DONT_HAVE_ACCOUNT),
        onPressed: () async {
          await _navigateToMe();
        },
      ),
    ];
  }

  Future _navigateToMe() async {
    await NavigationManager.pushClearBack(
      context,
      RegistrationPage(),
    );
  }

  Future _login() async {
    if (!_checkForm()) {
      return;
    }

    String error = await _controller.login(
      _emailController.text,
      _passwordController.text,
    );

    if (error != null) {
      await DialogsManager.showOkDialog(context: context, message: error);
      return;
    }

    await _navigateToMain(_controller.isAdminUser);
  }

  Future _navigateToMain(bool isAdmin) async {
    var page = HomePage();

    await NavigationManager.pushClearBack(
      context,
      page,
    );
  }

  bool _checkForm() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      return true;
    } else {
      return false;
    }
  }
}
