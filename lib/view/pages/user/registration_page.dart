import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tt_timezones/controllers/user_controller.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/utils/app_spaces.dart';
import 'package:tt_timezones/utils/debugging_manager.dart';
import 'package:tt_timezones/utils/dialogs_manager.dart';
import 'package:tt_timezones/utils/navigation_manager.dart';
import 'package:tt_timezones/utils/text_validator.dart';
import 'package:tt_timezones/view/shared_widgets/app_button.dart';
import 'package:tt_timezones/view/shared_widgets/app_consumer.dart';
import 'package:tt_timezones/view/shared_widgets/dismissible_keyboard.dart';
import 'package:tt_timezones/view/shared_widgets/user_type_widget.dart';

import '../home_page.dart';
import 'login_page.dart';

class RegistrationPage extends StatefulWidget {
  RegistrationPage({
    Key key,
  }) : super(key: key);

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  UserController _controller = locator<UserController>();

  final _formKey = GlobalKey<FormState>();

  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();
  bool _isAdmin = false;

  @override
  void initState() {
    super.initState();

    _emailController.text = DebuggingManager.USERNAME;
    _passwordController.text = DebuggingManager.PASSWORD;
    _confirmPasswordController.text = DebuggingManager.PASSWORD;

    _controller.logout().then((value) {});
  }

  @override
  Widget build(BuildContext context) {
    var consumer = ChangeNotifierProvider<UserController>(
      create: (context) => _controller,
      child: Consumer<UserController>(
        builder: (context, model, child) {
          return AppConsumer(
            value: model,
            builder: (context, value, child) {
              return _buildUi();
            },
          );
        },
      ),
    );

    return DismissibleKeyboard(
      context: context,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text(DictionaryService.REGISTER),
          ),
          body: consumer,
        ),
      ),
    );
  }

  Widget _buildUi() {
    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: _buildFields() + _buildButtons(),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildFields() {
    return [
      TextFormField(
        decoration: InputDecoration(
          labelText: DictionaryService.FIRST_NAME,
          suffixIcon: Icon(Icons.person),
        ),
        controller: _firstNameController,
        keyboardType: TextInputType.name,
        validator: (value) => TextValidator.validatePersonName(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      TextFormField(
        decoration: InputDecoration(
          labelText: DictionaryService.LAST_NAME,
          suffixIcon: Icon(Icons.perm_identity),
        ),
        controller: _lastNameController,
        keyboardType: TextInputType.name,
        validator: (value) => TextValidator.validatePersonName(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      TextFormField(
        decoration: InputDecoration(
          labelText: DictionaryService.EMAIL,
          suffixIcon: Icon(Icons.email),
        ),
        controller: _emailController,
        keyboardType: TextInputType.emailAddress,
        validator: (value) => TextValidator.validateEmail(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      TextFormField(
        decoration: InputDecoration(
          labelText: DictionaryService.PASSWORD,
          suffixIcon: Icon(Icons.security),
        ),
        controller: _passwordController,
        keyboardType: TextInputType.text,
        obscureText: true,
        validator: (value) => TextValidator.validatePassword(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      TextFormField(
        decoration: InputDecoration(
          labelText: DictionaryService.CONFIRM_PASSWORD,
          suffixIcon: Icon(Icons.security),
        ),
        controller: _confirmPasswordController,
        keyboardType: TextInputType.text,
        obscureText: true,
        validator: (value) => TextValidator.validateConfirmPassword(
          value,
          _passwordController.text,
        ),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      Text(DictionaryService.WHO_ARE_YOU),
      SizedBox(height: AppSpaces.MAIN_SPACE),
      UserTypeWidget(
        onSelected: (isAdmin) {
          _isAdmin = isAdmin;
        },
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
    ];
  }

  List<Widget> _buildButtons() {
    return [
      AppButton(
        text: DictionaryService.REGISTER,
        onPressed: () async {
          await _register();
        },
      ),
      TextButton(
        child: Text(DictionaryService.HAVE_ACCOUNT),
        onPressed: () async {
          await _navigateToMe();
        },
      ),
    ];
  }

  Future _navigateToMe() async {
    await NavigationManager.pushClearBack(
      context,
      LoginPage(),
    );
  }

  Future _register() async {
    if (!_checkForm()) {
      return;
    }

    String error = await _controller.register(
      _firstNameController.text,
      _lastNameController.text,
      _emailController.text,
      _passwordController.text,
      _isAdmin,
    );

    if (error != null) {
      await DialogsManager.showOkDialog(context: context, message: error);
      return;
    }

    await _navigateToMain(_controller.isAdminUser);
  }

  Future _navigateToMain(bool isAdmin) async {
    var page = HomePage();

    await NavigationManager.pushClearBack(
      context,
      page,
    );
  }

  bool _checkForm() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      return true;
    } else {
      return false;
    }
  }
}
