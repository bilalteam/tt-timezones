import 'package:flutter/material.dart';

class AppButton extends StatefulWidget {
  final String text;
  final VoidCallback onPressed;

  AppButton({
    Key key,
    @required this.text,
    this.onPressed,
  }) : super(key: key);

  @override
  _AppButtonState createState() => _AppButtonState();
}

class _AppButtonState extends State<AppButton> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Text(
        widget.text,
        style: Theme.of(context).textTheme.button,
      ),
      onPressed: () {
        widget.onPressed?.call();
      },
    );
  }
}
