import 'package:flutter/material.dart';
import 'package:provider/single_child_widget.dart';
import 'package:tt_timezones/controllers/base_controller.dart';

import 'center_progress.dart';

class AppConsumer<T extends BaseController> extends SingleChildStatelessWidget {
  final T value;
  final Widget Function(
    BuildContext context,
    T value,
    Widget child,
  ) builder;

  AppConsumer({
    Key key,
    @required this.value,
    @required this.builder,
    Widget child,
  }) : super(key: key, child: child);

  @override
  Widget buildWithChild(BuildContext context, Widget child) {
    BaseController baseController = value;

    if (baseController.busy) {
      return CenterProgress();
    }

    return builder(
      context,
      value,
      child,
    );
  }
}
