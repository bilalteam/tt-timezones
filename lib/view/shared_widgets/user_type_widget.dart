import 'package:flutter/material.dart';
import 'package:tt_timezones/utils/app_spaces.dart';

class UserTypeWidget extends StatefulWidget {
  final bool isAdmin;
  final Function(bool isAdmin) onSelected;

  UserTypeWidget({
    Key key,
    this.isAdmin = false,
    this.onSelected,
  }) : super(key: key);

  @override
  _UserTypeWidgetState createState() => _UserTypeWidgetState();
}

class _UserTypeWidgetState extends State<UserTypeWidget> {
  List<bool> _isSelected;

  @override
  void initState() {
    super.initState();

    _isSelected = [!widget.isAdmin, widget.isAdmin];
  }

  @override
  Widget build(BuildContext context) {
    return ToggleButtons(
      children: _buildToggles(),
      isSelected: _isSelected,
      onPressed: (int index) {
        setState(() {
          _selectJust(index);
        });

        widget.onSelected?.call(index == 1);
      },
    );
  }

  List<Widget> _buildToggles() {
    List<Widget> list = [
      _buildOneToggle('User', Icons.person),
      _buildOneToggle('Admin', Icons.supervisor_account),
    ];

    return list;
  }

  _buildOneToggle(String title, IconData iconData) {
    return Padding(
      padding: const EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            iconData,
            size: AppSpaces.MEDIUM_ICON,
          ),
          SizedBox(height: AppSpaces.HALF_SPACE),
          Text(title),
        ],
      ),
    );
  }

  void _selectJust(int index) {
    try {
      _trySelectJust(index);
    } catch (_) {}
  }

  void _trySelectJust(int index) {
    for (var i = 0; i < _isSelected.length; i++) {
      _isSelected[i] = false;
    }

    _isSelected[index] = true;
  }
}
