import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_analog_clock/flutter_analog_clock.dart';
import 'package:tt_timezones/utils/date_time_manager.dart';

class Clock extends StatefulWidget {
  final DateTime dateTime;

  Clock({
    Key key,
    @required this.dateTime,
  }) : super(key: key);

  @override
  _ClockState createState() => _ClockState();
}

class _ClockState extends State<Clock> {
  double _width;
  double _height;

  @override
  Widget build(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    _height = MediaQuery.of(context).size.height;

    return Center(
      child: _buildClock(),
    );
  }

  Widget _buildClock() {
    double size = min(_width, _height) / 2.0;
    double infoPadding = size / 2.0;

    String timeDay = DateTimeManager.formatDate(widget.dateTime, 'a');
    String day = DateTimeManager.formatDate(widget.dateTime, 'E');

    return FlutterAnalogClock.dark(
      child: Center(
        child: Padding(
          padding: EdgeInsets.only(top: infoPadding),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                timeDay,
                style: Theme.of(context).textTheme.subtitle2,
              ),
              Text(
                day,
                style: Theme.of(context).textTheme.subtitle2.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ],
          ),
        ),
      ),
      dateTime: widget.dateTime,
      width: size,
      height: size,
    );
  }
}
