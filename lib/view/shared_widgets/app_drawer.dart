import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tt_timezones/controllers/user_controller.dart';
import 'package:tt_timezones/locator.dart';
import 'package:tt_timezones/utils/app_colors.dart';
import 'package:tt_timezones/utils/app_spaces.dart';
import 'package:tt_timezones/utils/navigation_manager.dart';
import 'package:tt_timezones/view/pages/user/login_page.dart';

import 'app_consumer.dart';

class AppDrawer extends StatefulWidget {
  AppDrawer({Key key}) : super(key: key);

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  UserController _controller = locator<UserController>();

  @override
  void initState() {
    super.initState();

    _controller.fetchCurrentUser().then((value) => {});
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _buildDrawerHeader(context),
          _buildLogoutItem(context),
          _buildDivider(),
        ],
      ),
    );
  }

  Widget _buildDrawerHeader(BuildContext context) {
    return ChangeNotifierProvider<UserController>(
      create: (context) => _controller,
      child: Consumer<UserController>(
        builder: (context, model, child) {
          return AppConsumer(
            value: model,
            builder: (context, value, child) {
              return _buildUserAccountsDrawerHeader();
            },
          );
        },
      ),
    );
  }

  Widget _buildUserAccountsDrawerHeader() {
    String firstLetter = '';
    String fullName = '';
    String email = '';

    var currentUser = _controller.currentUserModel;

    if (currentUser != null) {
      firstLetter = currentUser.firstLetter;
      fullName = currentUser.fullName;
    }

    if (_controller.currentEmail != null) {
      email = _controller.currentEmail;
    }

    return UserAccountsDrawerHeader(
      accountName: Container(
        padding: const EdgeInsets.all(AppSpaces.QUARTER_SPACE),
        color: Theme.of(context).backgroundColor,
        child: Text(
          fullName,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ),
      accountEmail: Container(
        padding: const EdgeInsets.all(AppSpaces.QUARTER_SPACE),
        color: Theme.of(context).backgroundColor,
        child: Text(
          email,
          style: Theme.of(context).textTheme.bodyText2,
        ),
      ),
      currentAccountPicture: CircleAvatar(
        backgroundColor: AppColors.APP_COLOR,
        child: Text(
          firstLetter,
          style: Theme.of(context).textTheme.headline3.copyWith(
                fontWeight: FontWeight.bold,
              ),
        ),
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/images/header.png'),
        ),
      ),
    );
  }

  Widget _buildLogoutItem(BuildContext context) {
    return ListTile(
      title: Text(
        'Logout',
        style: TextStyle(color: Colors.white),
      ),
      leading: Icon(
        Icons.logout,
        color: Colors.white,
      ),
      onTap: () async {
        await NavigationManager.pushClearBack(context, LoginPage());
      },
    );
  }

  Widget _buildDivider() {
    return Divider(color: Colors.white);
  }
}
