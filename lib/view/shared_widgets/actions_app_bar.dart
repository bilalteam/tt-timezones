import 'package:flutter/material.dart';

class ActionsAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;

  const ActionsAppBar({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
