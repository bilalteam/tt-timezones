import 'package:flutter/material.dart';

class DismissibleKeyboard extends StatelessWidget {
  final BuildContext context;
  final Widget child;

  DismissibleKeyboard({
    Key key,
    @required this.child,
    @required this.context,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: child,
    );
  }
}
