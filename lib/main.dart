import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:tt_timezones/services/timezones_services.dart';
import 'package:tt_timezones/utils/app_colors.dart';

import 'locator.dart';
import 'services/dictionary_service.dart';
import 'view/pages/welcome_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await TimezonesServices.setup();

  setupLocator();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: DictionaryService.APP_NAME,
      theme: ThemeData(
        primarySwatch: AppColors.APP_COLOR,
        accentColor: AppColors.APP_COLOR,
        brightness: Brightness.dark,
      ),
      debugShowCheckedModeBanner: false,
      home: WelcomePage(),
    );
  }
}
