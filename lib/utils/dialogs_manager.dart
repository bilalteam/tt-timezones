import 'package:flutter/material.dart';
import 'package:tt_timezones/services/dictionary_service.dart';

class DialogsManager {
  static Future showOkDialog({
    @required BuildContext context,
    @required String message,
  }) async {
    await showDialog<void>(
      context: context,
      builder: (context) {
        return _buildTitleMessageAlertDialog(
          context,
          message,
        );
      },
    );
  }

  static AlertDialog _buildTitleMessageAlertDialog(
    BuildContext context,
    String message,
  ) {
    return AlertDialog(
      title: Text(DictionaryService.APP_NAME),
      content: SingleChildScrollView(
        child: Text(message),
      ),
      actions: <Widget>[
        _buildOkButton(context),
      ],
    );
  }

  static Widget _buildOkButton(BuildContext context) {
    return TextButton(
      child: Text(DictionaryService.OK),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  static Future showYesNoDialog({
    @required BuildContext context,
    @required String message,
    Function action,
  }) async {
    await showDialog<void>(
      context: context,
      builder: (context) {
        return _buildYesNoAlertDialog(
          context,
          message,
          action,
        );
      },
    );
  }

  static AlertDialog _buildYesNoAlertDialog(
    BuildContext context,
    String message,
    Function action,
  ) {
    return AlertDialog(
      title: Text(DictionaryService.APP_NAME),
      content: SingleChildScrollView(
        child: Text(message),
      ),
      actions: <Widget>[
        _buildYesButton(context, action),
        _buildNoButton(context),
      ],
    );
  }

  static Widget _buildYesButton(
    BuildContext context,
    Function action,
  ) {
    return TextButton(
      child: Text(DictionaryService.Yes),
      onPressed: () {
        action?.call();
      },
    );
  }

  static Widget _buildNoButton(BuildContext context) {
    return TextButton(
      child: Text(DictionaryService.No),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  static Future showWidgetDialog({
    @required BuildContext context,
    @required Widget child,
  }) async {
    await showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: child,
          ),
        );
      },
    );
  }
}
