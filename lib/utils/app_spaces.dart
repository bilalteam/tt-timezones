class AppSpaces {
  static const double NO_SPACE = 0.0;
  static const double MAIN_SPACE = 16.0;
  static const double DOUBLE_MAIN_SPACE = MAIN_SPACE * 2.0;
  static const double HALF_SPACE = MAIN_SPACE / 2.0;
  static const double QUARTER_SPACE = MAIN_SPACE / 4.0;

  static const double SMALL_ICON = 16.0;
  static const double MEDIUM_ICON = 48.0;
  static const double LARGE_ICON = 96.0;
}
