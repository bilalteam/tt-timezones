import 'package:flutter/material.dart';

class NavigationManager {
  static Future push(BuildContext context, Widget page) async {
    return await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => page),
    );
  }

  static Future pushNoDuplication(
      BuildContext context, Widget page, String routeName) async {
    Route currentRoute = ModalRoute.of(context);
    final currentRouteName = currentRoute?.settings?.name;

    if (routeName == null && currentRouteName == routeName) {
      return;
    }

    return await push(context, page);
  }

  static Future pushNamed(BuildContext context, String routeName,
      {Object arguments}) async {
    return await Navigator.pushNamed(context, routeName, arguments: arguments);
  }

  static Future pushNamedNoDuplication(BuildContext context, String routeName,
      {Object arguments}) async {
    Route currentRoute = ModalRoute.of(context);
    final currentRouteName = currentRoute?.settings?.name;

    if (routeName == null && currentRouteName == routeName) {
      return;
    }

    return await pushNamed(context, routeName, arguments: arguments);
  }

  static Future pushNoBack(BuildContext context, Widget page,
      {Object arguments}) async {
    return await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => page),
    );
  }

  static Future pushNamedNoBack(BuildContext context, String routeName,
      {Object arguments}) async {
    return await Navigator.pushReplacementNamed(context, routeName,
        arguments: arguments);
  }

  static Future pushNamedWithoutBack(BuildContext context, String routeName,
      {Object arguments}) async {
    return await Navigator.pushNamedAndRemoveUntil(
        context, routeName, (v) => false,
        arguments: arguments);
  }

  static Future pushClearBack(
    BuildContext context,
    Widget page,
  ) async {
    return await Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => page),
      (v) => false,
    );
  }

  static Future pushNamedClearBack(BuildContext context, String routeName,
      {Object arguments}) async {
    return await Navigator.pushNamedAndRemoveUntil(
        context, routeName, (v) => false,
        arguments: arguments);
  }

  static void pop(BuildContext context) {
    Navigator.pop(context);
  }

  static void popWithReturn(BuildContext context, Object value) {
    Navigator.pop(context, value);
  }
}
