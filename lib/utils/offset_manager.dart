import 'package:tt_timezones/services/dictionary_service.dart';
import 'package:tt_timezones/utils/duration_manager.dart';

class OffsetManager {
  static int offsetToMinutes(int offset) {
    double result = offset / Duration.millisecondsPerMinute;

    return result.toInt();
  }

  static String localDurationText(int offset) {
    DateTime now = DateTime.now();

    var nowDuration = now.timeZoneOffset;
    int nowOffset = nowDuration.inMilliseconds;

    int difference = offset - nowOffset;

    return offsetToDurationText(difference);
  }

  static String offsetToDurationText(int offset) {
    var duration = offsetToDuration(offset);
    int inHours = duration.inHours;
    String sign = inHours > 0 ? '+' : DictionaryService.EMPTY;

    String durationText = DurationManager.formatDuration(duration);

    return '$sign$durationText';
  }

  static Duration offsetToDuration(int offset) {
    return Duration(milliseconds: offset);
  }
}
