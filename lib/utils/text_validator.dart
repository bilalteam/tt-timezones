import 'package:tt_timezones/services/dictionary_service.dart';

class TextValidator {
  static String validateItem(dynamic value) {
    if (value == null || value == '') {
      return DictionaryService.ERROR_EMPTY;
    }

    return null;
  }

  static String validateName(String value) {
    if (value == null || value == '') {
      return DictionaryService.ERROR_EMPTY;
    }

    return null;
  }

  static String validatePersonName(String value) {
    if (value == null || value == '') {
      return DictionaryService.ERROR_EMPTY;
    }

    if (!isValidRegex(value, '^([a-zA-Z ]{3,30}\s*)+\$')) {
      return DictionaryService.ERROR_NOT_VALID_NAME;
    }

    return null;
  }

  static String validateEmail(String value) {
    try {
      return _tryValidateEmail(value);
    } catch (_) {
      return DictionaryService.ERROR_NOT_VALID_EMAIL;
    }
  }

  static String _tryValidateEmail(String value) {
    if (value == null || value == '') {
      return DictionaryService.ERROR_EMPTY;
    }

    if (!isValidRegex(value,
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")) {
      return DictionaryService.ERROR_NOT_VALID_EMAIL;
    }

    return null;
  }

  static String validatePassword(String value) {
    if (value == null || value == '') {
      return DictionaryService.ERROR_EMPTY;
    }

    if (value.length < 6) {
      return DictionaryService.ERROR_SHORT_PASSWORD;
    }

    return null;
  }

  static String validateConfirmPassword(String value, String sourcePassword) {
    if (value == null || value == '') {
      return DictionaryService.ERROR_EMPTY;
    }

    if (value != sourcePassword) {
      return DictionaryService.ERROR_NOT_MATCH_PASSWORD;
    }

    return null;
  }

  static bool isValidRegex(String value, String regex) {
    RegExp regExp = new RegExp(regex);

    return regExp.hasMatch(value);
  }
}
