import 'package:flutter/material.dart';

class AppColors {
  static const Color APP_COLOR = Colors.orange;
  static const Color POSITIVE_COLOR = Colors.green;
  static const Color NEGATIVE_COLOR = Colors.red;
}
