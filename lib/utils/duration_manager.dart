class DurationManager {
  static String formatDuration(Duration duration) => [
        duration.inHours,
        duration.inMinutes
      ].map((seg) => seg.remainder(60).toString().padLeft(2, '0')).join(':');
}
