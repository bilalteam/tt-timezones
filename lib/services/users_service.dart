import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class UsersService {
  final FirebaseAuth _instance = FirebaseAuth.instance;

  bool hasToken() {
    var user = getCurrentUser();

    return user != null;
  }

  String getCurrentUserId() {
    var user = getCurrentUser();

    return user == null ? null : user.uid;
  }

  User getCurrentUser() {
    return _instance.currentUser;
  }

  Future<UserCredential> signInWithEmailAndPassword({
    @required String email,
    @required String password,
  }) async {
    return await _instance.signInWithEmailAndPassword(
        email: email, password: password);
  }

  Future<UserCredential> createUserWithEmailAndPassword({
    @required String email,
    @required String password,
  }) async {
    return await _instance.createUserWithEmailAndPassword(
        email: email, password: password);
  }

  Future signOut() async {
    await _instance.signOut();
  }
}
