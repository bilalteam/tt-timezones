import 'package:flutter/services.dart';
import 'package:timezone/timezone.dart';

class TimezonesServices {
  static Future setup() async {
    var byteData = await rootBundle.load('packages/timezone/data/latest.tzf');

    initializeDatabase(byteData.buffer.asUint8List());
  }

  static Map<String, Location> get locations => timeZoneDatabase.locations;

  static void test() {
    locations.forEach((key, value) {
      print(value.toString());
    });
  }

  static TZDateTime convertByLocationName(
    DateTime source,
    String locationName,
  ) {
    return convertByLocation(source, getLocation(locationName));
  }

  static TZDateTime convertByLocation(
    DateTime source,
    Location location,
  ) {
    return TZDateTime.from(source, location);
  }

  static Location getLocationByName(String name) {
    return getLocation(name);
  }
}
