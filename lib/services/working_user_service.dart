import 'users_service.dart';

class WorkingUserService {
  final UsersService _usersService = UsersService();

  String _userId;

  String get userId {
    if (_userId == null) {
      _userId = _usersService.getCurrentUserId();
    }

    return _userId;
  }

  WorkingUserService() {
    _userId = _usersService.getCurrentUserId();
  }

  void selectUserId(String id) => _userId = id;

  void resetUserId() => _userId = null;
}
