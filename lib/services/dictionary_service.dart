class DictionaryService {
  static const String APP_NAME = 'T3';
  static const String EMPTY = '';
  static const String OK = 'OK';
  static const String Yes = 'Yes';
  static const String No = 'No';
  static const String TIMEZONE = 'Timezone';
  static const String TIMEZONES = 'Timezones';
  static const String USERS = 'Users';
  static const String ADMIN = 'Admin';
  static const String REGULAR = 'Regular';
  static const String LOGIN = 'Login';
  static const String BACK_TO_LOGIN = 'Back to login';
  static const String REGISTER = 'Register';
  static const String FIRST_NAME = 'First name';
  static const String LAST_NAME = 'Last name';
  static const String EMAIL = 'Email';
  static const String PASSWORD = 'Password';
  static const String CONFIRM_PASSWORD = 'Confirm password';
  static const String WHO_ARE_YOU = 'Who are you?';
  static const String HAVE_ACCOUNT = 'Have an account? Login';
  static const String DONT_HAVE_ACCOUNT = 'Dont have an account? Register';
  static const String ERROR_EMPTY = 'Should not be empty';
  static const String ERROR_NOT_VALID_NAME = 'Not valid name';
  static const String ERROR_NOT_VALID_EMAIL = 'Not valid email';
  static const String ERROR_SHORT_PASSWORD = 'Short password';
  static const String ERROR_NOT_MATCH_PASSWORD =
      'Password and confirm password does not match';
  static const String ERROR_ACCOUNT_DEACTIVATED =
      'Your account will be activated soon';
  static const String ERROR_NO_USER_FOUND = 'No user found for that email';
  static const String ERROR_WRONG_PASSWORD =
      'Wrong password provided for that user';
  static const String ERROR_WEAK_PASSWORD = 'The password provided is too weak';
  static const String ERROR_ACCOUNT_EXISTS =
      'The account already exists for that email';
  static const String ADD = 'Add';
  static const String SAVE = 'Save';
  static const String EDIT = 'Edit';
  static const String LOCAL = 'Local';
  static const String GMT = 'GMT';
  static const String CITY_NAME = 'City name';
  static const String SAVED_LOCATION = 'Saved location';
  static const String USER_TYPE = 'User type';
  static const String IS_ACTIVE = 'Is active?';
  static const String ADD_USER_ALERT =
      'You will be directed to the registration page, and you will be logged out, are you sure?';
  static const String SELECT_ADMIN_USER =
      'This is an admin user, you can edit and delete.';
}
